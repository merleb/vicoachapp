# Getting started

## Important commands:

### running

- `npx react-native run-ios`
- `npx react-native run-android`
- `npx react-native start`
- `npx react-native start --reset-cache`
- `react-native run-ios --simulator "iPhone 11 Pro Max"`
### checking environment

- `java -version` (needs to be 1.8.xx)
- `export JAVA_HOME=`/usr/libexec/java_home -v 1.8`` (MAC)
- `keytool -genkey -v -keystore debug.keystore -storepass android -alias androiddebugkey -keypass android -keyalg RSA -keysize 2048 -validity 10000`
### setup

- `adb reverse tcp:8001 tcp:8001` (Android command for connecting to localhost - app-server)
- `adb reverse tcp:8000 tcp:8000` (Android command for connecting to localhost - auth)
- `adb reverse tcp:3000 tcp:3000` (Android command for connecting to localhost - api)
- `MISSING` (iOS command for connecting to localhost)

### debugging

- `npm run lint` (run eslint over the whole project and logs the problems in terminal)
- `npm run lint-fix` (lints and fix auto-fixable problems)

## Things to note and to avoid:

- `npm install` after pulling
- Close metro when npm install is running
- Delete build folders
- Uninstall App on react-native update / npm install
- Do library updates on separate commits

## Eslint config

- eslint in within package.json

### Rules to be aware of

- "react-native/no-raw-text": this make sure you don't put raw text on the screen (in react native all text needs to be within a <Text></Text>),
  but if you make your own text component this rule gets confused but the solution is not to ignore it rather add your component to skip list:

```
"react-native/no-raw-text": [
        2,
        {
          "skip": [
            "LightText",
            "MediumText",
			"YOUR_COMPONENT_NAME"
          ]
        }
      ],
```

## good resources

- combining multiple styles in react-native: https://blog.laska.io/combining-multiple-styles-in-react-native
- offline first: https://pusher.com/tutorials/offline-react-native-part-1
