import React, { Component } from 'react'
import { View, AsyncStorage, Alert, FlatList, TouchableOpacity, Image, ScrollView, StatusBar } from 'react-native'
import { apiCall } from '../utils/api'
import { icon_weight, icon_running } from '../../assets'
import { Bar } from 'react-native-progress'
import MediumText from '../components/MediumText'
import LightText from '../components/LightText'
import PrimaryButton from '../components/PrimaryButton'
import styles from '../styles/ScreenStyles'
import { translate } from '../utils/translationHelper'

class HomeScreen extends Component {
	state = {
		user: null,
		weekTargets: [
			{ icon: icon_weight, type: 'measure_weight', key: '1' },
			{ icon: icon_running, type: 'running', key: '2', progress: 1.5, target: 3 }
		],
		dayTargets: [
			{ key: 3, type: 'breakfast', calories: 500, icon: null },
			{ key: 4, type: 'lunch', calories: 500, current: 'Nudelauflauf mit Hähnchenfleisch', icon: null },
			{ key: 5, type: 'dinner', calories: 400, current: 'Gefüllte Avocado mit Hüttenkäse und Roggenbrot', icon: null }
		]
	}

	componentDidMount() {
		this._getUserInfo()
	}

	async _getUserInfo() {
		const userInfo = await apiCall('/user-info')
		if (userInfo) {
			await AsyncStorage.setItem('user', JSON.stringify(userInfo))
		} else {
			Alert.alert(
				translate('authentification'),
				translate('no_user_info_alert'),
				[
					{ text: 'OK', onPress: () => this.props.navigation.navigate('Auth') }
				]
			)
		}
	}

	_handleOnPress(type) {
		if (['breakfast', 'lunch', 'dinner'].includes(type)) {
			this.props.navigation.navigate('Recipes', { type })
		}
	}

	_renderTarget = ({ item }) => {
		let title = translate(item.type)
		if (item.calories) {
			title += ` ${item.calories} kcal`
		}
		return (
			<TouchableOpacity style={styles.miniCard} onPress={() => this._handleOnPress(item.type)}>
				<Image style={styles.miniCardImage} source={item.icon} />
				<View style={{ flex: 1 }}>
					<View style={styles.miniCardDetails}>
						<MediumText>{title}</MediumText>
						{item.target
							? <LightText style={styles.miniCardDetailMax}>{item.target + ' ' + translate('hours')}</LightText>
							: null
						}
					</View>
					{item.current
						? <LightText style={styles.miniCardSubtitle}>{item.current}</LightText>
						: null
					}
					{item.progress
						? <View style={styles.miniCardProgress}>
							<Bar borderColor='#fff' progress={item.progress / item.target} color='#3EBD93' unfilledColor='#e4e7e9' borderRadius={8} width={150} height={10} />
							<LightText style={styles.miniCardProgressText}>{item.progress + ' ' + translate('hours_short')}</LightText>
						</View>
						: null
					}
				</View>
			</TouchableOpacity>
		)
	}

	_renderTargetSection = (title, data) => {
		return (
			<View>
				<View style={styles.miniCardsCategory}>
					<MediumText style={styles.miniCardsCategoryText}>{title}</MediumText>
				</View>
				<FlatList
					contentContainerStyle={{ alignSelf: 'center' }}
					data={data}
					keyExtractor={(item) => `${item.key}`}
					renderItem={this._renderTarget}
				/>
			</View>
		)
	}

	render() {
		const addCaloriesButtonTitle = translate('add_calories')
		return (
			<View>
				<ScrollView>
					{this._renderTargetSection(translate('weekly_targets'), this.state.weekTargets)}
					{this._renderTargetSection(translate('daily_targets'), this.state.dayTargets)}
					<View style={styles.addCaloriesButton}>
						<PrimaryButton title={addCaloriesButtonTitle} />
					</View>
				</ScrollView>
				<StatusBar barStyle="default" backgroundColor="#005a70" />
			</View>
		)
	}
}

export default HomeScreen