import React from 'react'
import { ActivityIndicator, AsyncStorage, StatusBar, View } from 'react-native'

class AuthLoadingScreen extends React.Component {
	componentDidMount() {
		this._bootstrapAsync()
	}

	_bootstrapAsync = async () => {
		const authToken = await AsyncStorage.getItem('authToken')

		this.props.navigation.navigate(authToken ? 'App' : 'Auth')
	};

	render() {
		return (
			<View>
				<ActivityIndicator />
				<StatusBar barStyle="default" />
			</View>
		)
	}
}

export default AuthLoadingScreen