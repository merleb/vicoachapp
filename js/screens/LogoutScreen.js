import React from 'react'
import {Alert} from 'react-native'
import authBrowserInstance from '../utils/auth'
import { translate } from '../utils/translationHelper'

export default class SettingsScreen extends React.Component {
	componentDidMount() {
		this._confirmLogout()
	}
	_confirmLogout() {
		Alert.alert(
			translate('logout'),
			translate('logout_alert'),
			[
				{text: translate('close'), onPress: () => this.props.navigation.navigate('Settings')},
				{text: translate('logout'), onPress: () => authBrowserInstance.logout()}
			]
		)
	}

	render() {
		return null
	}
}