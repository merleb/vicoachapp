import React, { Component } from 'react'
import { View, AsyncStorage, FlatList } from 'react-native'
import Header from '../components/Header'
import ListItem from '../components/ListItem'
import { apiCall } from '../utils/api'
import styles from '../styles/ScreenStyles'
import { translate } from '../utils/translationHelper'
import { measurementTypes } from '../utils/measurements'

const _ = require('lodash')

class ProfileScreen extends Component {
	state = {
		measurements: []
	}

	async componentDidMount() {
		const userFromStorage = await AsyncStorage.getItem('user')
		await this._getUserMeasurements(JSON.parse(userFromStorage))
	}

	async _getUserMeasurements(userFromStorage) {
		const measurements = await apiCall('/user/measurements', { user_id: userFromStorage._id })
		this.setState({ measurements })
	}

	_renderItem = ({ item }) => {
		const measurements = this.state.measurements
		const measurementsForType = measurements.filter((measurement) => {
			return item.type === measurement.type
		})
		return <ListItem
			onPress={() => this.props.navigation.navigate('MeasurementType', { name: item.name, measurements: measurementsForType })}
			title={item.name}
		/>
	}

	render() {
		const measurements = this.state.measurements
		let visibleMeasurementTypes = measurements.map((measurement) => {
			for (let i = 0; i < measurementTypes.length; i++) {
				if (measurementTypes[i].type === measurement.type) {
					return measurementTypes[i]
				}
			}
		})
		visibleMeasurementTypes = _.uniq(visibleMeasurementTypes)
		return <View style={styles.container}>
			<Header title={translate('measurements')} navigation={this.props.navigation} redirectTo={'Home'} />
			<View style={styles.profileList}>
				<FlatList
					data={visibleMeasurementTypes}
					renderItem={this._renderItem}
					keyExtractor={item => item._id}
				/>
			</View>
		</View>
	}
}

export default ProfileScreen