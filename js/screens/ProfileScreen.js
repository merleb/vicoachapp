import React from 'react'
import { View, AsyncStorage, FlatList } from 'react-native'
import Header from '../components/Header'
import ListItem from '../components/ListItem'
import moment from 'moment'
import styles from '../styles/ScreenStyles'
import { translate, localeDateFormat } from '../utils/translationHelper'

class ProfileScreen extends React.Component {
	state = {
		userData: null
	}

	async componentDidMount() {
		const userFromStorage = await AsyncStorage.getItem('user')
		const userData = this._getProfileDetails(JSON.parse(userFromStorage))
		this.setState({ userData: userData })
	}

	_getProfileDetails(userFromStorage) {
		const userData = [
			{ title: translate('firstname'), value: userFromStorage.firstname },
			{ title: translate('lastname'), value: userFromStorage.lastname },
			{ title: translate('email'), value: userFromStorage.email },
			// {title: 'Geschlecht', value: userFromStorage.gender},
			{ title: translate('height'), value: userFromStorage.height },
			{ title: translate('weight'), value: userFromStorage.weight },
			{ title: translate('birthday'), value: moment(userFromStorage.birthday).format(localeDateFormat()) },
			{ title: translate('postcode'), value: userFromStorage.postcode }
		]

		return userData
	}

	_renderItem = ({ item }) => {
		return <ListItem title={item.title} value={item.value} />
	}

	render() {
		return <View style={styles.container}>
			<Header title={translate('profile')} navigation={this.props.navigation} redirectTo={'Settings'} />
			{this.state.userData
				? <View style={styles.profileList}>
					<FlatList
						data={this.state.userData}
						renderItem={this._renderItem}
						keyExtractor={item => item.title}
					/>
				</View>
				: null
			}
		</View>
	}
}

export default ProfileScreen