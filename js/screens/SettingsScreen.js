import React, {Component} from 'react'
import { View, FlatList } from 'react-native'
import ListItem from '../components/ListItem'
import {imprint, privacy} from '../../assets/index'
import Header from '../components/Header'
import { translate } from '../utils/translationHelper'
import styles from '../styles/ScreenStyles'

class SettingsScreen extends Component {
	constructor(props) {
		super(props)
		this.settingsData = [
			{
				title: translate('profile'),
				navigateTo: 'Profile'
			},
			{
				title: translate('imprint'),
				navigateTo: 'TextView',
				navigateProps: {html: imprint, title: translate('imprint'), redirect: 'Settings'}
			},
			{
				title: translate('privacy_policy'),
				navigateTo: 'TextView',
				navigateProps: {html: privacy, title: translate('privacy_policy'), redirect: 'Settings'}
			},
			{
				title: translate('logout'),
				navigateTo: 'Logout'
			}
		]
	}

		_renderItem = ({item}) => {
			return <ListItem onPress={() => this.props.navigation.navigate(item.navigateTo, item.navigateProps)} title={item.title}/>
		}

		render() {
			return <View style={styles.container}>
				<Header title={translate('settings')} navigation={this.props.navigation} redirectTo={'Home'}/>
				<View style={styles.profileList}>
					<FlatList
						data={this.settingsData}
						renderItem = {this._renderItem}
						keyExtractor={item => item.title}
					/>
				</View>
			</View>
		}
}

/* const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	list: {
		flex: 1,
		justifyContent: 'center',
		paddingLeft: 10
	}
}) */

export default SettingsScreen