import React, {Component} from 'react'
import { View, FlatList, TouchableOpacity, Image, ScrollView } from 'react-native'
import MediumText from '../components/MediumText'
import Header from '../components/Header'
import styles from '../styles/ScreenStyles'
import { translate } from '../utils/translationHelper'
import Icon from 'react-native-vector-icons/MaterialIcons'
import LightText from '../components/LightText'

const difficulties = {
	1: 'easy',
	2: 'medium',
	3: 'difficult'
}
class HomeScreen extends Component {
		state = {
			currentView: 'breakfast',
			recipes: [
				{ key: '1', types: ['breakfast', 'lunch'], title: 'Käse und Schinken Omelette', difficulty: 1, time: '15 Minuten', icon: null},
				{ key: '2', types: ['breakfast', 'lunch'], title: 'Gefüllte Avocado mit Hüttenkäse und Roggenbrot', difficulty: 1, time: '5 Minuten', icon: null},
				{ key: '3', types: ['breakfast'], title: 'Erdbeerpfannkuchen', difficulty: 3, time: '20 Minuten', icon: null},
				{ key: '4', types: ['breakfast'], title: 'Buttermilch Pfannkuchen', difficulty: 2, time: '15 Minuten', icon: null},
				{ key: '5', types: ['dinner'], title: 'Spargelsalat', difficulty: 2, time: '20 Minuten', icon: null}
			]

		}

		componentDidMount() {
			const view = this.props.navigation.getParam('type')
			this._changeViewHandler(view)
		}

		_changeViewHandler(newView) {
			this.setState({currentView: newView})
		}

		_renderRecipeCard = ({ item }) => {
			if (item.types.includes(this.state.currentView)) {
				const difficultyText = difficulties[item.difficulty]
				return <TouchableOpacity style={styles.miniCard}>
					<Image style={styles.miniCardImage} source={item.icon}/>
					<View style={{flex: 1}}>
						<View style={styles.miniCardDetails}>
							<MediumText>{item.title}</MediumText>
						</View>
						<View style={styles.tagBar}>
							<View style={[styles.tag, styles[`${difficultyText}Tag`]]}>
								<LightText style={styles[`${difficultyText}Text`]}>{translate(difficultyText)}</LightText>
							</View>
							<View style={[styles.tag, styles.timeTag]}>
								<Icon name='alarm' size={15} color='#3171e0'/>
								<LightText style={styles.recipeDurationText}>{item.time}</LightText>
							</View>
						</View>
					</View>
				</TouchableOpacity>
			} else {
				null
			}
		}

		render() {
			const title = translate('recipes')
			return <ScrollView>
				<Header
					title={title}
					navigation={this.props.navigation}
					redirectTo={'Home'}
					activeView = {this.state.currentView}
					views={['breakfast', 'lunch', 'dinner']}
					settings={'RecipesSettings'}
					changeView={this._changeViewHandler.bind(this)}/>
				<FlatList
					contentContainerStyle={{alignSelf: 'center'}}
					data={this.state.recipes}
					keyExtractor={(item) => item.key}
					renderItem={this._renderRecipeCard}
				/>
			</ScrollView>
		}
}

export default HomeScreen