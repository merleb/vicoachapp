import React, { Component } from 'react'
import { View } from 'react-native'
import { WebView } from 'react-native-webview'
import Header from '../components/Header'
import styles from '../styles/ScreenStyles'

class TextViewScreen extends Component {
	render() {
		const htmlToRender = this.props.navigation.getParam('html')
		const title = this.props.navigation.getParam('title')
		const redirect = this.props.navigation.getParam('redirect')
		return (
			<View style={styles.container}>
				<Header title={title} navigation={this.props.navigation} redirectTo={redirect} />
				<View style={styles.container}>
					<WebView
						source={{ html: htmlToRender }}
						domStorageEnabled
						javaScriptEnabled
					/>
				</View>
			</View>
		)
	}
}

export default TextViewScreen