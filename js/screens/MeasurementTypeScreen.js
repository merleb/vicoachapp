import React, { Component } from 'react'
import { View, FlatList } from 'react-native'
import Header from '../components/Header'
import ListItem from '../components/ListItem'
import styles from '../styles/ScreenStyles'
import { localeDateFormat } from '../utils/translationHelper'
import InAppBrowser from 'react-native-inappbrowser-reborn'
import moment from 'moment'

class MeasurementTypeScreen extends Component {
	state = {
		name: '',
		measurements: []
	}

	componentDidMount() {
		const name = this.props.navigation.getParam('name')
		const measurements = this.props.navigation.getParam('measurements')
		this.setState({ name, measurements })
	}

	async _openMeasurement(type, id) {
		const linkTo = 'https://labs.vicoach.io/1.1.18/#/de-DE/' + type + '/' + id
		await InAppBrowser.open(linkTo, {
			// iOS Properties
			dismissButtonStyle: 'cancel',
			preferredBarTintColor: 'gray',
			preferredControlTintColor: 'white',
			// Android Properties
			showTitle: true,
			toolbarColor: '#005a70',
			secondaryToolbarColor: 'black',
			enableUrlBarHiding: true,
			enableDefaultShare: true,
			forceCloseOnRedirection: true
		})
	}

	_renderItem = ({ item }) => {
		return <ListItem
			onPress={(() => this._openMeasurement(this.state.name, item._id))}
			title={this.state.name}
			value={moment(item.timestamp).format(localeDateFormat())}
		/>
	}

	render() {
		return <View style={styles.container}>
			<Header title={this.state.name} navigation={this.props.navigation} redirectTo={'Measurements'} />
			{this.state.measurements
				? <View style={styles.profileList}>
					<FlatList
						data={this.state.measurements}
						renderItem={this._renderItem}
						keyExtractor={item => item._id}
					/>
				</View>
				: null
			}
		</View>
	}
}

export default MeasurementTypeScreen