import React, { Component } from 'react'
import { View, TouchableOpacity, Linking, AsyncStorage, Alert, Text, Platform, StatusBar } from 'react-native'
import authBrowserInstance from '../utils/auth'
import CookieManager from 'react-native-cookies'
import LightText from '../components/LightText'
import { translate } from '../utils/translationHelper'
import styles from '../styles/ScreenStyles'
import BigPrimaryButton from '../components/BigPrimaryButton'
import SecondaryButton from '../components/SecondaryButton'
import { imprint, privacy } from '../../assets/index'

export default class SignInScreen extends Component {
	state = {
		needAuth: false
	}

	async componentDidMount() {
		await CookieManager.clearAll()
		if (Platform.OS === 'android') {
			const url = await Linking.getInitialURL()
			await this.handleOpenURL(url)
		} else {
			Linking.addEventListener('url', (event) => this.handleOpenURL(event.url))
		}
	}

	handleOpenURL = async (url) => {
		if (url) {
			const route = url.replace(/.*?:\/\//g, '')
			let cookies = route.match(/\/([^/]+)\/?$/)
			if (cookies) {
				cookies = cookies[1]
				if (cookies === 'error') {
					this.setState({ needAuth: true })
					Alert.alert(
						translate('login'),
						translate('login_failed_alert'),
						[{ text: 'OK' }]
					)
				} else if (cookies !== 'logout') {
					const koaSess = cookies.split('&')[0]
					const koaSessSig = cookies.split('&')[1]
					await AsyncStorage.multiSet([['koaSess', koaSess], ['koaSessSig', koaSessSig]])
					await authBrowserInstance.getToken()
					this.props.navigation.navigate('App')
				} else {
					this.props.navigation.navigate('Auth')
				}

				if (Platform.OS === 'ios') {
					authBrowserInstance.close()
				}
			}
		}
	}

	componentWillUnmount() { // C
		Linking.removeEventListener('url', this.handleOpenURL)
	}

	_renderLoginInfo(title, url) {
		const handleClick = () => {
			Linking.canOpenURL(url).then(supported => {
				if (supported) {
					Linking.openURL(url)
				} else {
					console.log("Don't know how to open URI: " + url)
				}
			})
		}
		return <SecondaryButton title={title} onPress={handleClick} />
	}

	render() {
		return <View style={styles.loginContainer}>
			<View style={styles.logo}>
				<Text style={styles.slash}>/</Text>
				<LightText style={styles.vicoach}>vi</LightText>
				<LightText style={[styles.vicoach, { fontWeight: 'bold' }]}>coach</LightText>
			</View>
			<View style={{ flex: 3 }}>
				<BigPrimaryButton title={translate('login')} onPress={() => authBrowserInstance.login()} />
			</View>
			<View style={styles.loginInfo}>
				{this._renderLoginInfo(translate('vicoach_info'), 'https://www.cardioscan.de/vicoach/')}
				{this._renderLoginInfo(translate('checkpoint_info'), 'https://www.cardioscan.de/check-finder/')}
			</View>
			<View style={styles.policies}>
				<TouchableOpacity onPress={() => this.props.navigation.navigate('TextView', { html: imprint, title: translate('imprint'), redirect: 'SignIn' })}>
					<LightText style={{ fontSize: 13 }}>{translate('imprint')}</LightText>
				</TouchableOpacity>
				<TouchableOpacity onPress={() => this.props.navigation.navigate('TextView', { html: privacy, title: translate('privacy_policy'), redirect: 'SignIn' })}>
					<LightText style={{ fontSize: 13 }}>{translate('privacy_policy')}</LightText>
				</TouchableOpacity>
			</View>
			<StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
		</View>
	}
}
