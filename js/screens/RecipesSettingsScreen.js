import React, {Component} from 'react'
import {View, ScrollView, TouchableOpacity } from 'react-native'
import MediumText from '../components/MediumText'
import Header from '../components/Header'
import styles from '../styles/ScreenStyles'
import {translate} from '../utils/translationHelper'
import LightText from '../components/LightText'
import ChooseType from '../components/ChooseType'
// import CheckBox from '@react-native-community/checkbox'
// import { Checkbox } from 'react-native-paper'
import Checkbox from '../components/ui/MyCheckbox'

const difficulties = {
	1: 'easy',
	2: 'medium',
	3: 'difficult'
}
const dietary_preferences = {
	1: 'none',
	2: 'vegan',
	3: 'vegetarian'
}
const shopping_preferences = {
	1: 'supermarket',
	2: 'week_market'
}
class RecipesSettingsScreen extends Component {
		state = {
			dietary_preference: '1',
			difficulties: ['1', '2'],
			shopping_preference: '1',
			showDietaryPreferences: false,
			showShoppingPreferences: false
		}

		_onChangeDifficulty(stage) {
			const currentDifficulties = this.state.difficulties
			const index = currentDifficulties.indexOf(stage)
			if (index > -1) {
				currentDifficulties.splice(index, 1)
			} else {
				currentDifficulties.push(stage)
			}
			this.setState({difficulties: currentDifficulties})
		}

		_renderDifficultyCheckboxes() {
			// use switch for ios
			const checkboxArray = []
			for (const stage in difficulties) {
				checkboxArray.push(<View key={stage} style={styles.checkboxView}>
					<Checkbox isChecked={this.state.difficulties.includes(stage.toString())} onPress={() => this._onChangeDifficulty(stage)}/>
					<View style={styles.checkboxText}>
						<LightText>{translate(difficulties[stage])}</LightText>
					</View>
				</View>)
			}
			return checkboxArray
		}

		_toggleDietaryPreferences() {
			const showDietaryPreferences = this.state.showDietaryPreferences
			this.setState({showDietaryPreferences: !showDietaryPreferences, showShoppingPreferences: false})
		}

		_toggleShoppingPreferences() {
			const showShoppingPreferences = this.state.showShoppingPreferences
			this.setState({showShoppingPreferences: !showShoppingPreferences, showDietaryPreferences: false})
		}

		_renderTypeHeader(typeToChoose, typeChosen, toggleTypeChooser) {
			return <View>
				<MediumText style={styles.settingsHeader}>{typeToChoose}</MediumText>
				<TouchableOpacity style={styles.chooseTypeHeader} onPress={toggleTypeChooser}>
					<LightText>{typeChosen}</LightText>
				</TouchableOpacity>
			</View>
		}

		_renderTypeChooser(display, toggleDisplay, types, onValueChange) {
			return <ChooseType
				display={display}
				toggleDisplay={toggleDisplay}
				types={types}
				onValueChange={onValueChange}>
			</ChooseType>
		}

		render() {
			const title = translate('recipes')
			const preferredDiet = translate(dietary_preferences[this.state.dietary_preference])
			const preferredShop = translate(shopping_preferences[this.state.shopping_preference])
			return <ScrollView style={{flex: 1}}>
				<Header title={title} redirectTo={'Recipes'} navigation={this.props.navigation}/>
				{this._renderTypeHeader(translate('dietary_preference'), preferredDiet, () => this._toggleDietaryPreferences())}
				<MediumText style={styles.settingsHeader}>{translate('cooking_expenses')}</MediumText>
				{this._renderDifficultyCheckboxes()}
				{this._renderTypeHeader(translate('shopping_preference'), preferredShop, () => this._toggleShoppingPreferences())}
				{this._renderTypeChooser(
					this.state.showDietaryPreferences,
					this._toggleDietaryPreferences.bind(this),
					dietary_preferences,
					(itemValue) => {
						this.setState({dietary_preference: itemValue, showDietaryPreferences: false})
					}
				)}
				{this._renderTypeChooser(
					this.state.showShoppingPreferences,
					this._toggleShoppingPreferences.bind(this),
					shopping_preferences,
					(itemValue) => {
						this.setState({shopping_preference: itemValue, showShoppingPreferences: false})
					}
				)}
			</ScrollView>
		}
}

export default RecipesSettingsScreen