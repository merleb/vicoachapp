import React, { Component } from 'react'
import { View, TouchableOpacity, Dimensions, StyleSheet } from 'react-native'
import MediumText from '../components/MediumText'
import styles from '../styles/ScreenStyles'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { translate } from '../utils/translationHelper'

class AddActivityScreen extends Component {
	_renderActionButton(title, icon, transformXValue, navigateTo) {
		const dynamicStyles = StyleSheet.create({
			floatingActionCurve: {
				transform: [{
					translateX: -(Dimensions.get('window').width / 7.5) + transformXValue
				}]
			}
		})
		console.log(transformXValue)
		return (
			<View style={[
				styles.floatingAction,
				dynamicStyles.floatingActionCurve
			]}>
				<MediumText style={styles.floatingActionTitle}>{title}</MediumText>
				<TouchableOpacity style={styles.floatingActionButton} onPress={() => this.props.navigation.navigate(navigateTo)}>
					<Icon name={icon} size={25} />
				</TouchableOpacity>
			</View>
		)
	}
	render() {
		return (
			<View style={styles.floatingActionButtonScreen}>
				<TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
					<Icon name={'cancel'} size={40} />
				</TouchableOpacity>
				{this._renderActionButton(translate('live_tracking'), 'navigation', 0, '')}
				{this._renderActionButton(translate('add_training_unit'), 'fitness-center', 20, 'AddTrainingUnit')}
				{this._renderActionButton(translate('measure_weight'), 'cake', 60, '')}
				{this._renderActionButton(translate('measure_heart_rate'), 'favorite', 100, '')}
			</View>
		)
	}
}

export default AddActivityScreen