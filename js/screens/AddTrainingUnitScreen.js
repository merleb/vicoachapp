import React, { Component } from 'react'
import { View, TouchableOpacity } from 'react-native'
import MediumText from '../components/MediumText'
import styles from '../styles/ScreenStyles'
import Header from '../components/Header'
import {translate} from '../utils/translationHelper'
import DateTimePicker from '@react-native-community/datetimepicker'
import moment from 'moment'
import PrimaryButton from '../components/PrimaryButton'
import ChooseType from '../components/ChooseType'
import LightText from '../components/LightText'

const sport_types = {
	1: 'boxing',
	2: 'running',
	3: 'yoga'
}

class AddTrainingUnitScreen extends Component {
		state = {
			sport_type: '1',
			pickTime: false,
			pickedTime: null,
			showSportTypes: false
		}
		componentDidMount() {
			this._setInitialTime()
		}
		_toggleTimePicker() {
			const pickTime = this.state.pickTime
			this.setState({pickTime: !pickTime})
		}
		_toggleSportTypes() {
			const showSportTypes = this.state.showSportTypes
			this.setState({showSportTypes: !showSportTypes})
		}
		_setInitialTime() {
			const time = moment().toDate()

			time.setHours(0)
			time.setMinutes(0)
			time.setSeconds(0)
			time.setMilliseconds(0)

			this.setState({pickedTime: time.getTime()})
		}
		_setTrainingTime(e) {
			this._toggleTimePicker()
			if (e.type === 'dismissed') {
				this._setInitialTime()
			} else {
				const pickedTime = e.nativeEvent.timestamp
				this.setState({pickedTime})
			}
		}
		render () {
			const title = translate('add_training_unit')
			const sportChosen = translate(sport_types[this.state.sport_type])
			return <View style={{flex: 1}}>
				<Header title={title} redirectTo={'AddActivity'} navigation={this.props.navigation}/>
				<View style={styles.trainingUnitContainer}>
					<View>
						<MediumText style={styles.settingsHeader}>{translate('choose_sport')}</MediumText>
						<TouchableOpacity style={styles.chooseTypeHeader} onPress={() => this._toggleSportTypes()}>
							<LightText>{sportChosen}</LightText>
						</TouchableOpacity>
					</View>
					<TouchableOpacity style={styles.timeInput} onPress={this._toggleTimePicker.bind(this)}>
						<View><MediumText style={styles.timeText}>{moment(this.state.pickedTime).format('HH:mm')}</MediumText></View>
					</TouchableOpacity>
					{this.state.pickTime
						? <DateTimePicker
							value={this.state.pickedTime}
							mode='time'
							display='spinner'
							onChange={this._setTrainingTime.bind(this)}
						/>
						: null }
				</View>
				<View style={styles.continueButton}>
					<ChooseType
						display={this.state.showSportTypes}
						toggleDisplay={this._toggleSportTypes.bind(this)}
						types={sport_types}
						onValueChange={(itemValue) => {
							this.setState({sport_type: itemValue, showSportTypes: false}
							)
						}}/>
					{!this.state.showSportTypes
						? <PrimaryButton title={translate('continue')}/>
						: null
					}
				</View>
			</View>
		}
}

export default AddTrainingUnitScreen