import React from 'react'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { createStackNavigator } from 'react-navigation-stack'
import HomeScreen from './screens/HomeScreen'
import SettingsScreen from './screens/SettingsScreen'
import SignInScreen from './screens/SignInScreen'
import AuthLoadingScreen from './screens/AuthLoadingScreen'
import LogoutScreen from './screens/LogoutScreen'
import MainHeader from './components/MainHeader'
import TextViewScreen from './screens/TextViewScreen'
import ProfileScreen from './screens/ProfileScreen'
import AddActivityScreen from './screens/AddActivityScreen'
import AddTrainingUnitScreen from './screens/AddTrainingUnitScreen'
import RecipesScreen from './screens/RecipesScreen'
import MeasurementsScreen from './screens/MeasurementsScreen'
import MeasurementTypeScreen from './screens/MeasurementTypeScreen'
import RecipesSettingsScreen from './screens/RecipesSettingsScreen'
import MyPlanScreen from './screens/MyPlanScreen'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { translate, localeDateFormat } from './utils/translationHelper'
import moment from 'moment'
import LightText from './components/LightText'

const tabBarItems = [
	{
		name: 'Home',
		icon: 'home',
		label: 'home'
	},
	{
		name: 'Settings',
		icon: 'settings-applications',
		label: 'settings'
	},
	{
		name: 'AddActivity',
		icon: 'add-circle'
	},
	{
		name: 'Measurements',
		icon: 'timeline',
		label: 'measurements'
	},
	{
		name: 'MyPlan',
		icon: 'date-range',
		label: 'my_plan'
	}
]

const AuthStack = createStackNavigator({
	SignIn: {
		screen: SignInScreen,
		navigationOptions: {
			header: null
		}
	}
})

const MainStack = createStackNavigator({
	Home: {
		screen: HomeScreen,
		navigationOptions: {
			// eslint-disable-next-line react/display-name
			header: () => {
				const title = translate('today')
				return <MainHeader title={title} subtitle={moment(new Date()).format(localeDateFormat())} />
			}
		}
	},
	Recipes: {
		screen: RecipesScreen,
		navigationOptions: {
			header: null
		}
	},
	RecipesSettings: {
		screen: RecipesSettingsScreen,
		navigationOptions: {
			header: null
		}
	}
}, {
	cardStyle: { backgroundColor: '#f5f7fa' }
})

const MeasurementsStack = createStackNavigator({
	Measurements: MeasurementsScreen,
	MeasurementType: MeasurementTypeScreen
},
	{
		defaultNavigationOptions: {
			header: null
		}
	})

const AddActivityStack = createStackNavigator({
	AddActivity: AddActivityScreen,
	AddTrainingUnit: AddTrainingUnitScreen
},
	{
		defaultNavigationOptions: {
			header: null
		}
	},
	{
		cardStyle: { backgroundColor: '#f5f7fa' }
	})

const MyPlanStack = createStackNavigator({
	MyPlan: MyPlanScreen
},
	{
		defaultNavigationOptions: {
			header: null
		}
	},
	{
		cardStyle: { backgroundColor: '#f5f7fa' }
	})

const tabBarVisible = (navigation) => {
	const { routes } = navigation.state

	let showTabBar = true
	routes.forEach((route) => {
		if (route.routeName === 'AddActivity') {
			showTabBar = false
		}
	})
	return showTabBar
}

const AppNavigator = createBottomTabNavigator({
	Home: MainStack,
	Measurements: MeasurementsStack,
	AddActivity: AddActivityStack,
	MyPlan: MyPlanStack,
	Settings: SettingsScreen
},
	{
		defaultNavigationOptions: ({ navigation }) => ({
			// It's ok to suppress this error because react navigation will be refactored to version 5
			// eslint-disable-next-line react/display-name
			tabBarIcon: ({ tintColor }) => {
				const { routeName } = navigation.state
				const tabBarItem = tabBarItems.find(item => item.name === routeName)
				const iconName = tabBarItem.icon
				return <Icon name={iconName} size={25} color={tintColor} />
			},
			tabBarLabel: ({ tintColor }) => {
				const { routeName } = navigation.state
				const tabBarItem = tabBarItems.find(item => item.name === routeName)
				const label = tabBarItem.label
				if (label) {
					return <LightText style={{ alignSelf: 'center', fontSize: 10, color: tintColor }}>{translate(label)}</LightText>
				}
			}
		}),
		navigationOptions: ({ navigation }) => ({
			tabBarVisible: tabBarVisible(navigation)
		}),
		tabBarOptions: {
			activeTintColor: '#005a70',
			inactiveTintColor: '#7b8794'
		}
	})

const SwitchNavigator = createSwitchNavigator(
	{
		AuthLoading: AuthLoadingScreen,
		App: AppNavigator,
		Auth: AuthStack,
		Logout: LogoutScreen,
		TextView: TextViewScreen,
		Profile: ProfileScreen
	},
	{
		initialRouteName: 'AuthLoading'
	}
)

export default createAppContainer(SwitchNavigator)