import { StyleSheet } from 'react-native'

export default StyleSheet.create({
	activeColor: {
		color: '#2186eb'
	},

	activeViewBarItem: {
		borderBottomColor: '#fff',
		borderBottomWidth: 2
	},

	activeViewTitle: {
		alignSelf: 'center',
		color: '#fff',
		marginBottom: 10
	},

	bigPrimaryButton: {
		alignItems: 'center',
		backgroundColor: '#005a70',
		borderRadius: 8,
		height: 60,
		justifyContent: 'center',
		padding: 10,
		width: '100%'
	},

	chooseTypeButtons: {
		flexDirection: 'row',
		justifyContent: 'flex-end'
	},

	chooseTypeContainer: {
		height: 200
	},

	chooseTypeItem: {
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center',
		marginTop: 10
	},

	header: {
		alignItems: 'center',
		backgroundColor: '#005a70',
		flexDirection: 'row',
		height: 50,
		width: '100%'
	},

	headerContainer: {
		alignContent: 'flex-start',
		backgroundColor: '#005a70',
		flexDirection: 'column'
	},

	headerContent: {
		flex: 1,
		flexDirection: 'row'
	},

	headerProgress: {
		flexDirection: 'row',
		marginTop: 10
	},

	headerSettings: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end'
	},

	headerSubtitle: {
		color: '#fff'
	},

	headerTitle: {
		color: '#fff',
		fontSize: 20
	},

	headerViewBar: {
		flexDirection: 'row',
		justifyContent: 'space-evenly'
	},

	inactiveViewTitle: {
		alignSelf: 'center',
		color: '#7b8794',
		marginBottom: 10
	},

	itemText: {
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'space-between',
		width: '100%'
	},

	itemTitle: {
		fontSize: 15,
		margin: 10
	},

	itemValue: {
		fontSize: 12,
		marginRight: 10
	},

	listItem: {
		borderBottomColor: '#7b8794',
		borderBottomWidth: 1,
		flexDirection: 'row',
		height: 50
	},

	mainHeader: {
		alignItems: 'flex-start',
		backgroundColor: '#005a70',
		paddingBottom: 10,
		paddingLeft: 20,
		paddingTop: 20
	},

	mainHeaderTitle: {
		color: '#fff',
		fontSize: 30
	},

	primaryButton: {
		alignItems: 'center',
		backgroundColor: '#005a70',
		borderRadius: 8,
		height: 40,
		justifyContent: 'center',
		paddingBottom: 10,
		paddingLeft: 20,
		paddingRight: 20,
		paddingTop: 10,
		width: 'auto'
	},

	primaryButtonTitle: {
		color: '#fff',
		fontSize: 20
	},

	secondaryButton: {
		alignItems: 'center',
		backgroundColor: '#fff',
		borderColor: '#005a70',
		borderRadius: 8,
		borderWidth: 1,
		height: 40,
		justifyContent: 'center',
		padding: 10,
		width: '100%'
	},

	secondaryButtonTitle: {
		color: '#005a70',
		fontSize: 12,
		fontWeight: 'bold'
	},

	viewBarItem: {
		flex: 1,
		width: '100%'
	}
})