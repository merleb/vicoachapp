import { StyleSheet } from 'react-native'

export default StyleSheet.create({
	addCaloriesButton: {
		alignSelf: 'center',
		marginTop: 10
	},

	checkboxText: {
		alignItems: 'center',
		backgroundColor: '#fff',
		borderBottomColor: '#7b8794',
		borderBottomWidth: 1,
		flex: 1,
		flexDirection: 'row',
		height: 50
	},

	checkboxView: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flexDirection: 'row',
		paddingLeft: 15
	},

	chooseTypeHeader: {
		backgroundColor: '#fff',
		borderBottomColor: '#7b8794',
		borderBottomWidth: 1,
		height: 50,
		justifyContent: 'center',
		marginTop: 10,
		paddingLeft: 15
	},

	container: {
		flex: 1
	},

	continueButton: {
		alignSelf: 'center',
		paddingBottom: 20
	},

	difficultTag: {
		backgroundColor: '#ffe8d9',
		width: 80
	},

	difficultText: {
		color: '#de3a11'
	},

	easyTag: {
		backgroundColor: '#c6f7e2',
		width: 80
	},

	easyText: {
		color: '#0c6b58'
	},

	floatingAction: {
		alignItems: 'center',
		alignSelf: 'center',
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginBottom: 20,
		width: 150
	},

	floatingActionButton: {
		alignItems: 'center',
		backgroundColor: '#e4e7e9',
		borderRadius: 30,
		height: 60,
		justifyContent: 'center',
		width: 60
	},

	floatingActionButtonScreen: {
		flex: 1,
		flexDirection: 'column-reverse',
		alignItems: 'center'
	},

	floatingActionTitle: {
		marginRight: 10
	},

	loginContainer: {
		alignItems: 'center',
		flex: 1,
		paddingLeft: 15,
		paddingRight: 15
	},

	loginInfo: {
		flex: 1.5,
		justifyContent: 'space-between'
	},

	logo: {
		alignItems: 'center',
		flexDirection: 'row',
		flex: 4,
		justifyContent: 'flex-start'
	},

	mediumTag: {
		backgroundColor: '#fff3c4',
		width: 80
	},

	mediumText: {
		color: '#cb6e17'
	},

	miniCard: {
		alignItems: 'center',
		backgroundColor: '#fff',
		borderRadius: 4,
		elevation: 5,
		flex: 0,
		flexDirection: 'row',
		height: 80,
		justifyContent: 'flex-start',
		margin: 5,
		padding: 10,
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 2
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		width: 320
	},

	miniCardDetailMax: {
		fontSize: 10,
		marginLeft: 5
	},

	miniCardDetails: {
		alignItems: 'center',
		flexDirection: 'row',
		height: 10,
		margin: 10
	},

	miniCardImage: {
		backgroundColor: '#e4e7e9',
		borderRadius: 30,
		height: 60,
		width: 60
	},

	miniCardProgress: {
		alignItems: 'center',
		flexDirection: 'row',
		height: 10,
		margin: 10
	},

	miniCardProgressText: {
		fontSize: 10,
		marginLeft: 5
	},

	miniCardSubtitle: {
		fontSize: 10,
		marginLeft: 10
	},

	miniCardsCategory: {
		alignSelf: 'center',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-start',
		marginTop: 10,
		marginBottom: 10,
		width: 320
	},

	miniCardsCategoryText: {
		fontSize: 18
	},

	policies: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		width: '100%'
	},

	profileList: {
		flex: 1,
		justifyContent: 'center',
		paddingLeft: 10
	},

	recipeDurationText: {
		color: '#3171e0',
		marginLeft: 5
	},

	settingsHeader: {
		fontSize: 20,
		fontWeight: 'bold',
		marginLeft: 15,
		marginTop: 20
	},

	slash: {
		color: '#e1e000',
		fontSize: 60,
		marginBottom: 100,
		marginTop: 50
	},

	tag: {
		alignItems: 'center',
		borderRadius: 10,
		height: 20,
		justifyContent: 'center',
		marginLeft: 10,
		marginRight: 10,
		padding: 10
	},

	tagBar: {
		flexDirection: 'row',
		justifyContent: 'flex-start',
		marginTop: 10
	},

	timeInput: {
		alignItems: 'center',
		alignSelf: 'center',
		borderRadius: 4,
		elevation: 5,
		height: 80,
		justifyContent: 'center',
		marginTop: 20,
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 2
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		width: 180
	},

	timeTag: {
		backgroundColor: '#f0f5ff',
		flexDirection: 'row'
	},

	timeText: {
		fontSize: 20
	},

	trainingUnitContainer: {
		flex: 1,
		justifyContent: 'flex-start'
	},

	vicoach: {
		color: '#005a70',
		fontSize: 60,
		marginBottom: 100,
		marginTop: 50
	}
})