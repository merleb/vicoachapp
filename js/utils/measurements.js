export const measurementTypes = [
	{
		'type': 0,
		'name': 'metabolicscan'
	},
	{
		'type': 1,
		'name': 'bodyscan'
	},
	{
		'type': 2,
		'name': 'plan'
	},
	{
		'type': 3,
		'name': 'performancescan'
	},
	{
		'type': 4,
		'name': 'cardioscan'
	},
	{
		'type': 5,
		'name': 'bodyscanpro'
	},
	{
		'type': 6,
		'name': 'lifestylescan'
	},
	{
		'type': 7,
		'name': 'bodyscanseca'
	}
]