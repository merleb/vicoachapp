import { AsyncStorage, Linking } from 'react-native'
import InAppBrowser from 'react-native-inappbrowser-reborn'
import { REACT_APP_SERVER_ENDPOINT_URL } from 'react-native-dotenv'
const _ = require('lodash')

class AuthBrowser {
	constructor() {
		// const serverUrl = 'https://labs.vicoach.io'
		// const serverUrl = 'http:/192.168.254.245:8001'
		this.serverUrl = REACT_APP_SERVER_ENDPOINT_URL
		this.inAppBrowser = InAppBrowser
	}

	getHeaderWithCookies = async () => {
	// eslint-disable-next-line no-undef
		const headers = new Headers()

		const koaSess = await AsyncStorage.getItem('koaSess')
		const koaSessSig = await AsyncStorage.getItem('koaSessSig')

		headers.append('Content-Type', 'application/json; charset=utf-8')
		headers.append('Connection', 'keep-alive')
		headers.append('Cookie', 'koa:sess=' + koaSess + '; ' + ' koa:sess.sig=' + koaSessSig + ';')

		return headers
	}

	getToken = async () => {
		const headers = await this.getHeaderWithCookies()

		const me = await fetch(this.serverUrl + '/me', {
			method: 'Get',
			headers: headers
		})

		if (me) {
			const authToken = await me.json()
			if (!_.isEmpty(authToken)) {
				await AsyncStorage.setItem('authToken', authToken)
			}
		}
	}

	login = async () => {
		const url = this.serverUrl + '/login-app'
		try {
			if (await this.inAppBrowser.isAvailable()) {
				await this.inAppBrowser.open(url, {
				// iOS Properties
					dismissButtonStyle: 'cancel',
					preferredBarTintColor: '#005a70',
					preferredControlTintColor: 'white',
					modalEnabled: true,
					modalPresentationStyle: 'popup',
					// Android Properties
					showTitle: true,
					toolbarColor: '#005a70',
					secondaryToolbarColor: 'black',
					enableUrlBarHiding: true,
					enableDefaultShare: true,
					forceCloseOnRedirection: true
				})
			} else Linking.openURL(url)
		} catch (error) {
			Linking.openURL(url)
		}
	}

	refreshToken = async () => {
		let newToken
		const headers = await this.getHeaderWithCookies()
		const refresh = await fetch(this.serverUrl + '/refresh', {
			method: 'Get',
			headers: headers
		})

		if (refresh) {
			const result = await refresh.json()
			if (result.error === false) {
				newToken = result.payload
				await AsyncStorage.setItem('authToken', newToken)
			} else {
				newToken = null
			}
		}
		return newToken
	}

	logout = async () => {
		const headers = await this.getHeaderWithCookies()

		await AsyncStorage.removeItem('authToken')
		await AsyncStorage.removeItem('koaSess')
		await AsyncStorage.removeItem('koaSessSig')
		await AsyncStorage.removeItem('user')

		const url = this.serverUrl + '/logout-app'
		try {
			if (await this.inAppBrowser.isAvailable()) {
				await this.inAppBrowser.open(url, {
					// iOS Properties
					dismissButtonStyle: 'cancel',
					preferredBarTintColor: '#005a70',
					preferredControlTintColor: 'white',
					modalEnabled: true,
					modalPresentationStyle: 'fullScreen',
					// Android Properties
					showTitle: true,
					toolbarColor: '#005a70',
					secondaryToolbarColor: 'black',
					enableUrlBarHiding: true,
					enableDefaultShare: true,
					forceCloseOnRedirection: true,
					headers: headers
				})
			} else Linking.openURL(url)
		} catch (error) {
			Linking.openURL(url)
		}
	}

	close = () => {
		this.inAppBrowser.close()
	}
}

const authBrowserInstance = new AuthBrowser()

export default authBrowserInstance