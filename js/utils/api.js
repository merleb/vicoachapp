import { AsyncStorage } from 'react-native'
import authBrowserInstance from './auth'
import { REACT_APP_API_ENDPOINT_URL } from 'react-native-dotenv'

// const apiUrl = 'http:/192.168.254.245:9999/'
// const apiUrl = 'https://api-labs.vicoach.io'
const apiUrl = REACT_APP_API_ENDPOINT_URL

async function getHeaderWithToken() {
	// eslint-disable-next-line no-undef
	const headers = new Headers()

	const token = await AsyncStorage.getItem('authToken')

	headers.append('Content-Type', 'application/json; charset=utf-8')
	headers.append('Connection', 'keep-alive')
	headers.append('Authorization', 'Bearer ' + token)

	return headers
}

export async function apiCall(path, data) {
	let result
	const user = await AsyncStorage.getItem('user')
	const headers = await getHeaderWithToken()
	const response = await fetch(apiUrl + path, {
		method: data ? 'Post' : 'Get',
		headers: headers,
		user: user || null,
		body: JSON.stringify(data)
	})
	if (response.status === 200) {
		result = await response.json()
	} else if (response.status === 401) {
		const newToken = await authBrowserInstance.refreshToken()
		if (newToken) {
			const newHeader = await getHeaderWithToken()
			const newResponse = await fetch(apiUrl + path, {
				method: 'Get',
				headers: newHeader
			})
			if (newResponse) {
				result = await newResponse.json()
			}
		} else {
			return null
		}
	}
	return result.payload
}