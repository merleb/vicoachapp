import React, { Component } from 'react'
import { View, TouchableOpacity, ScrollView } from 'react-native'
import { translate } from '../utils/translationHelper'
import styles from '../styles/ComponentStyles'
import LightText from './LightText'
// import { ScrollView } from 'react-native-gesture-handler' 
import MediumText from './MediumText'

class AddTrainingUnitScreen extends Component {
	state = {
		activeType: ''
	}

	_addPickerItems() {
		const types = this.props.types
		const items = []
		for (const type in types) {
			items.push(<TouchableOpacity onPress={() => this.setState({ activeType: type.toString() })} style={styles.chooseTypeItem} key={type.toString()}>
				<LightText style={[type === this.state.activeType ? styles.activeColor : null, { fontSize: 20 }]}>{translate(types[type])}</LightText>
			</TouchableOpacity>)
		}
		return items
	}

	_renderButton(title, onPress) {
		return <TouchableOpacity onPress={onPress}>
			<MediumText style={[styles.activeColor, { padding: 10 }]}>{title}</MediumText>
		</TouchableOpacity>
	}

	render() {
		// Styled within styles.xml (in android/app/src/main/res/values), ios??
		if (this.props.display) {
			return <View style={styles.chooseTypeContainer}>
				<View style={styles.chooseTypeButtons}>
					{this._renderButton(translate('back'), this.props.toggleDisplay)}
					{this._renderButton(translate('continue'), () => this.props.onValueChange(this.state.activeType))}
				</View>
				<ScrollView>
					{this._addPickerItems()}
				</ScrollView>
			</View>
		} else return null
	}
}

export default AddTrainingUnitScreen