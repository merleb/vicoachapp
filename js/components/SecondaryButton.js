import React, { Component } from 'react'
import { TouchableOpacity, View } from 'react-native'
import LightText from './LightText'
import styles from '../styles/ComponentStyles'

class SecondaryButton extends Component {
	render() {
		return (
			<View style={{ flexDirection: 'row' }}>
				<TouchableOpacity style={styles.secondaryButton} onPress={this.props.onPress}>
					<LightText style={styles.secondaryButtonTitle}>{this.props.title}</LightText>
				</TouchableOpacity>
			</View>
		)
	}
}

export default SecondaryButton