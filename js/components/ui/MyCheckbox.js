import React from 'react'
// import moduleName from '@react-native-community/checkbox'
import { Checkbox as CheckboxPaper } from 'react-native-paper'

const MyCheckbox = ({ isChecked, onPress }) => (
	<CheckboxPaper color="#005a70" status={isChecked ? 'checked' : 'unchecked'} onPress={onPress}/>
)

export default MyCheckbox