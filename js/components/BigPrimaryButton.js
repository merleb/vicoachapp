import React, { Component } from 'react'
import { TouchableOpacity, View } from 'react-native'
import LightText from './LightText'
import styles from '../styles/ComponentStyles'

class BigPrimaryButton extends Component {
	render() {
		return (
			<View style={{ flexDirection: 'row' }}>
				<TouchableOpacity style={styles.bigPrimaryButton} onPress={this.props.onPress}>
					<LightText style={styles.primaryButtonTitle}>{this.props.title}</LightText>
				</TouchableOpacity>
			</View>
		)
	}
}

export default BigPrimaryButton