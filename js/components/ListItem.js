import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import MediumText from './MediumText'
import LightText from './LightText'
import styles from '../styles/ComponentStyles'

class ListItem extends React.PureComponent {
	render() {
		return (
			<View style={styles.listContainer}>
				<TouchableOpacity onPress={this.props.onPress} style={styles.listItem}>
					<View style={styles.itemText}>
						<LightText style={styles.itemTitle}>
							{this.props.title}
						</LightText>
						{this.props.value
							? <MediumText style={styles.itemValue}>
								{this.props.value}
							</MediumText>
							: null
						}
					</View>
				</TouchableOpacity>
			</View>)
	}
}

export default ListItem