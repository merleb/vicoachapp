import React from 'react'
import { Text } from 'react-native'

const MediumText = (props) => (
	<Text {...props} style={[{fontFamily: 'Gotham-Medium'}, props.style]}>{props.children}</Text>
)

export default MediumText
