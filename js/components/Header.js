import React, { Component } from 'react'
import { View, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import MediumText from './MediumText'
import styles from '../styles/ComponentStyles'
import { translate } from '../utils/translationHelper'

class Header extends Component {
	_getViews(views) {
		const renderedViews = []
		for (let i = 0; i < views.length; i++) {
			const active = this.props.activeView === views[i]
			renderedViews.push(
				<TouchableOpacity key={views[i]} style={[styles.viewBarItem, active ? styles.activeViewBarItem : null]} onPress={() => this.props.changeView(views[i])}>
					<MediumText style={active ? styles.activeViewTitle : styles.inactiveViewTitle}>{translate(views[i])}</MediumText>
				</TouchableOpacity>
			)
		}
		return renderedViews
	}

	_renderViewBar(views) {
		return <View style={styles.headerViewBar}>
			{this._getViews(views)}
		</View>
	}

	render() {
		return (<View style={styles.headerContainer}>
			<View style={styles.header}>
				<Icon.Button
					onPress={() => this.props.navigation.navigate(this.props.redirectTo)}
					name='chevron-left' size={30}
					backgroundColor='#005a70'
					color='#2186eb'
				/>
				<MediumText style={styles.headerContent}>
					<MediumText style={styles.headerTitle}>
						{this.props.title}
					</MediumText>
				</MediumText>
				{this.props.settings
					? <View style={styles.headerSettings}>
						<Icon.Button
							onPress={() => this.props.navigation.navigate(this.props.settings)}
							name='more-horiz' size={30}
							backgroundColor='#005a70'
							color='#2186eb' />
					</View>
					: null
				}
			</View>
			{this.props.views
				? this._renderViewBar(this.props.views)
				: null
			}
		</View>
		)
	}
}

export default Header