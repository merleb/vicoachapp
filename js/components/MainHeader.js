import React from 'react'
import { View, Text } from 'react-native'
import { Bar } from 'react-native-progress'
import MediumText from './MediumText'
import LightText from './LightText'
import styles from '../styles/ComponentStyles'

class MainHeader extends React.PureComponent {
	render() {
		return (
			<View style={styles.mainHeader}>
				<Text style={styles.mainHeaderText}>
					<MediumText style={styles.mainHeaderTitle}>
						{this.props.title} &nbsp;
					</MediumText>
					<LightText style={styles.headerSubtitle}>
						{this.props.subtitle}
					</LightText>
				</Text>
				<View style={styles.headerProgress}>
					<Bar borderColor='#e4e7e9' unfilledColor='#e4e7e9' width={320} height={8} borderRadius={8} />
				</View>
			</View>
		)
	}
}

export default MainHeader