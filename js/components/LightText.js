import React from 'react'
import { Text } from 'react-native'

const LightText = (props) => (
	<Text {...props} style={[{fontFamily: 'Gotham-Light'}, props.style]}>{props.children}</Text>
)

export default LightText
