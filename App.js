import React, { Component } from 'react'
import AppNavigator from './js/AppNavigator'
import { setI18nConfig } from './js/utils/translationHelper'
import { Provider as PaperProvider } from 'react-native-paper'

/*
Require cycle happens when a package A require B and B require C but C require A
It happens from time to time that react-native has a bug that does this
These 2 lines suppress that warning on the device screen 
*/
import { YellowBox } from 'react-native';
import { SafeAreaView } from 'react-navigation';
YellowBox.ignoreWarnings(['Require cycle:']);
/* 
If the bug is not there anymore get rid of them
*/

export default class App extends Component {
	constructor(props) {
		super(props)
		setI18nConfig()
	}
	render() {
		return (
			<PaperProvider>
				<SafeAreaView forceInset={{ bottom: 'never' }} style={{ flex: 1, backgroundColor: '#005a70' }}>
					<AppNavigator />
				</SafeAreaView>
			</PaperProvider>
		)
	}
}
